#[macro_use]
extern crate lazy_static;
extern crate time;

mod day1;
mod day2;
mod day3;

use std::env;
use std::collections;

lazy_static! {
    static ref PART_FUNCS: collections::HashMap<(i8, i8), fn ()> = {
        let mut p: collections::HashMap<(i8, i8), fn ()> = collections::HashMap::new();

        p.insert((1, 1), day1::day1p1);
        p.insert((1, 2), day1::day1p2);

        p.insert((2, 1), day2::day2p1);
        p.insert((2, 2), day2::day2p2);

        p.insert((3, 1), day3::day3p1);
        p.insert((3, 2), day3::day3p2);

        p
    };
}

fn main() {
    let args: Vec<i8> = env::args().skip(1).map(|arg: String| {arg.parse().unwrap()}).collect();

    let jour = args.get(0).unwrap_or(&-1);
    let part = args.get(1).unwrap_or(&-1);

    for curr_jour in 1..26 {
        if *jour != -1 && *jour != curr_jour {
            continue
        }

        for curr_part in 1..3 {
            if *part != -1 && *part != curr_part {
                continue
            }

            let my_part = PART_FUNCS.get(&(curr_jour, curr_part));
            
            if my_part.is_none() {
                continue
            }

            let my_part = my_part.unwrap();

            println!("Lancement de {}:{}", curr_jour, curr_part);

            let start = time::now();
            my_part();
            println!("Temps : {}", time::now() - start);

            println!()
        }
    }
}