extern crate regex;

use std::fs;
use std::collections;

lazy_static! {
    static ref PARSER: regex::Regex = {
        regex::Regex::new(r"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").expect("Can't compile regex")
    };
}

struct FabricPiece {
    id: isize,
    pos: (isize, isize),
    size: (isize, isize),
}

impl FabricPiece {
    fn build_from_captures(c: &regex::Captures) -> FabricPiece {
        FabricPiece::build_fabric(c[1].parse().unwrap(),
                                 (c[2].parse().unwrap(), c[3].parse().unwrap()),
                                 (c[4].parse().unwrap(), c[5].parse().unwrap()))
    }

    fn build_fabric(id: isize, pos: (isize, isize), size: (isize, isize)) -> FabricPiece {
        FabricPiece {
            id, pos, size
        }
    }
}

pub fn day3p1() {
    let input = fs::read_to_string("inputs/input3.txt").expect("Impossible de lire le fichier d'entrée");
    let mut f_map: collections::HashMap<isize, collections::HashMap<isize, isize>> = collections::HashMap::new();

    let mut tot = 0;

    for l in input.lines() {
        let piece = PARSER.captures(l).expect("Can't parse input");
        let piece = FabricPiece::build_from_captures(&piece);

        for x in piece.pos.0..piece.pos.0+piece.size.0 {
            for y in piece.pos.1..piece.pos.1+piece.size.1 {
                let row = f_map.entry(x).or_insert_with(collections::HashMap::new);
                let cell = row.entry(y).or_insert(0);
                *cell += 1;

                if *cell == 2 {
                    tot += 1;
                }
            }
        }
    }

    println!("Total : {}", tot);
}

pub fn day3p2() {
    let input = fs::read_to_string("inputs/input3.txt").expect("Impossible de lire le fichier d'entrée");
    let mut f_map: collections::HashMap<isize, collections::HashMap<isize, isize>> = collections::HashMap::new();

    let mut valid: collections::HashSet<isize> = collections::HashSet::new();

    for l in input.lines() {
        let piece = PARSER.captures(l).expect("Can't parse input");
        let piece = FabricPiece::build_from_captures(&piece);
        valid.insert(piece.id);

        for x in piece.pos.0..piece.pos.0+piece.size.0 {
            for y in piece.pos.1..piece.pos.1+piece.size.1 {
                let row = f_map.entry(x).or_insert_with(collections::HashMap::new);

                let cell = row.entry(y).or_insert(piece.id);

                if cell != &piece.id {
                    valid.remove(&row[&y]);
                    valid.remove(&piece.id);
                }
            }
        }
    }

    if valid.len() != 1 {
        panic!("Une seule claim doit être valide")
    }
    println!("Restant : {}", valid.iter().next().unwrap());
}