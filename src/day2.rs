use std::fs;

pub fn day2p1() {
    let input = fs::read_to_string("inputs/input2.txt").expect("Impossible de lire le fichier d'entrée");

    let mut nb2 = 0;
    let mut nb3 = 0;

    for curr_l in input.lines() {
        let mut array_l = [0; 26];

        for c in curr_l.chars() {
            array_l[(c as usize) - ('a' as usize)] += 1;
        }

        if array_l.iter().any(|x| *x == 2) {
            nb2 += 1;
        }

        if array_l.iter().any(|x| *x == 3) {
            nb3 += 1;
        }
    }

    println!("Résultat : {}", nb2*nb3);
}

pub fn day2p2() {
    let input = fs::read_to_string("inputs/input2.txt").expect("Impossible de lire le fichier d'entrée");

    let mut curr_start_index = 0;

    for first in input.lines() {
        curr_start_index += 1;
        for second in input.lines().skip(curr_start_index) {
            if first == second {
                continue
            }

            let mut f_iter = first.chars();
            let mut s_iter = second.chars();
            let mut cur_diff: i32 = -1;

            for i in 0..first.len()-1 {
                let cf = f_iter.next().unwrap();
                let cs = s_iter.next().unwrap();

                if cf == cs {
                    continue;
                }

                if cur_diff == -1 {
                    cur_diff = i as i32;
                } else {
                    cur_diff = -1;
                    break;
                }
            }

            if cur_diff != -1 {
                let fin: String = first.char_indices().filter(|(pos, _)| *pos != cur_diff as usize).map(|(_, my_c)| my_c).collect();
                println!("Resultat 1 : {}", fin);
                return;
            }
        }
    }
}