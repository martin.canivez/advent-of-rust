use std::fs;
use std::collections;

pub fn day1p1() {
    let input = fs::read_to_string("inputs/input1.txt").expect("Impossible de lire le fichier d'entrée");

    let lines: Vec<i32> = input.split_terminator('\n').map(|x: &str| x.parse().expect("Erreur de parsing d'un nombre")).collect();

    let mut running_count = 0;

    for curr_count in lines {
        running_count += curr_count;
    }

    println!("Fréquence : {}", running_count);
}

pub fn day1p2() {
    let input = fs::read_to_string("inputs/input1.txt").expect("Impossible de lire le fichier d'entrée");

    let lines: Vec<i32> = input.split_terminator('\n')
                               .map(|x: &str| x.parse()
                                               .expect("Erreur de parsing d'un nombre"))
                               .collect();

    let mut running_count = 0;
    let mut seen_freqs = collections::HashSet::new();
    seen_freqs.insert(0);

    'dance: loop {
        for curr_freq in &lines {
            running_count += curr_freq;

            if seen_freqs.contains(&running_count) {
                break 'dance;
            }

            seen_freqs.insert(running_count);
        }
    }

    println!("Fréquence : {}", running_count);
}